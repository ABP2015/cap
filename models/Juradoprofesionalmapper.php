<?php
require_once('core/PDOConnection.php');

require_once (__DIR__ . "/Juradoprofesional.php");

/**
 * Class Juradoprofesionalmapper
 *
 * Interfaz para el acceso a la base de dato de las entidades de Juradoprofesional
 *
 * @author Rafael Castillo alcaraz
 */
class Juradoprofesionalmapper {
	/**
	 * Referencia a la conexion PDO
	 * 
	 * @var PDO
	 */
	private $db;
	
	public function __construct() {
		$this->db = PDOConnection::getInstance ();
	}

	/**
	 * Recupera un juradoprofesional
	 *
	 * @param Juradoprofesional $jurado El jurado con id que se quiere recuperar de la base de datos
	 * @throws PDOException si existe un error con la base de datos
	 * @return JuradoProfesional El miembro del jurado recuperado de la base de datos. Devuelve null si se ha producido un error.
	 */
	public function recuperarJuradoProfesional($idJurado) {
		$stmt = $this->db->prepare ( "SELECT * FROM juradoprofesional WHERE idjuradoprofesional=?" );
		$stmt->execute ( array (
			$idJurado 
			) );
		$juradoProfesional = $stmt->fetch ( PDO::FETCH_ASSOC );
		if ($juradoProfesional != null) {
			return new Juradoprofesional ( $juradoProfesional ["idjuradoprofesional"], $juradoProfesional ["nombre"], $juradoProfesional ["usuario"], $juradoProfesional ["email"], $juradoProfesional ["password"], $juradoProfesional ["telefono"], $juradoProfesional ["foto"], $juradoProfesional ["descripcion"] );
		} else {
			return NULL;
		}
	}

	/**
	* Recupera todos los jurados profesionales
	*
	* @throws PDOException si existe error con la base de datos
	* @return $jurados El array de jurados recuperados de la base de datos
	*/

	public function recuperarTodosLosJurados(){
		$stmt = $this->db->prepare ( "SELECT * FROM juradoprofesional");
		$stmt->execute();
		$juradosRecuperados = $stmt->fetchAll();
		$jurados = array();
		foreach ($juradosRecuperados as $jurado) {
			$jurados[] = new Juradoprofesional ( $jurado ["idjuradoprofesional"], $jurado ["nombre"], $jurado ["usuario"], $jurado ["email"], $jurado ["password"], $jurado ["telefono"], $jurado ["foto"], $jurado ["descripcion"] );
		}
		return $jurados;
	}

	/**
	 * Actualiza un miembro del jurado
	 *
	 * @param Juradoprofesional $jurado jurado con la id y los datos que se desean actualizar
	 * @param int $idConcurso identificador del concurso asociado al jurado
	 * @param int $idAdministrador identificador del administrador asociado al jurado
	 * @throws PDOException si existe un error con la base de datos	 
	 * @throws Exception si se actualiza mas de una tupla en la base de datos
	 * @return boolean. Devuelve true (1) si se ha producido la actualizacion, false (0 
	 */
	public function actualizarJuradoProfesional($jurado, $idConcurso, $idAdministrador) {
		$stmt = $this->db->prepare ( "UPDATE juradoprofesional SET usuario=?,nombre=?,email=?,password=?,foto=?,descripcion=?,telefono=?,concurso_idconcurso=?,administrador_idadministrador=? WHERE idjuradoprofesional=?" );
		$stmt->execute ( array (
			$jurado->get_login (),
			$jurado->get_nombre (),
			$jurado->get_email (),
			$jurado->get_password (),
			$jurado->get_foto (),
			$jurado->get_descripcion (),
			$jurado->get_telefono (),
			$idConcurso,
			$idAdministrador,
			$jurado->get_id () 
			) );
		$count = $stmt->rowCount ();
		switch ($count) {
			case 0 :
			return false;
			break;
			case 1 :
			return true;
			break;
			default :
			//throw new Exception ( "Error al realizar la actualizacion en la BD" );
			return false;
			break;
		}
	}
	
	/**
	 * Elimina un miembro del jurado
	 *
	 * @param Juradoprofesional $jurado jurado con la id que se desea eliminar
	 * @throws PDOException si existe un error con la base de datos
	 * @throws Exception si se elimina mas de una tupla en la base de datos
	 * @return boolean. Devuelve true (1) si se ha producido la eliminacion, false (0) en caso contrario
	 */
	public function borrarJuradoProfesional($jurado) {
		$stmt = $this->db->prepare ( "DELETE from juradoprofesional WHERE idjuradoprofesional=?" );
		$stmt->execute ( array (
			$jurado->get_id () 
			) );
		$count = $stmt->rowCount ();
		switch ($count) {
			case 0 :
			return false;
			break;
			case 1 :
			return true;
			break;
			default :
			//throw new Exception ( "Error al realizar la eliminación en la BD" );
			return false;
			break;
		}
	}
	/**
	 * Inserta un nuevo miembro del jurado
	 *
	 * @param Juradoprofesional $jurado jurado con la id y los datos que se desean insertar
	 * @param int $idConcurso identificador del concurso asociado al jurado
	 * @param int $idAdministrador identificador del administrador asociado al jurado
	 * @throws PDOException si existe un error con la base de datos
	 * @throws Exception si se inserta mas de una tupla en la base de datos
	 * @return boolean. Devuelve true (1) si se ha producido la insercion, false (0) en caso contrario
	 */
	public function insertarJuradoProfesional($jurado, $idConcurso, $idAdministrador) {
		$stmt = $this->db->prepare ( "INSERT INTO juradoprofesional(usuario, nombre, email, password, foto, descripcion, telefono, concurso_idconcurso, administrador_idadministrador) values (?,?,?,?,?,?,?,?,?,?)" );
		$stmt->execute ( array (
			$jurado->get_login (),
			$jurado->get_nombre (),
			$jurado->get_email (),
			$jurado->get_password (),
			$jurado->get_foto (),
			$jurado->get_descripcion (),
			$jurado->get_telefono (),
			$idConcurso,
			$idAdministrador 
			) );
		$count = $stmt->rowCount ();
		switch ($count) {
			case 0 :
			return false;
			break;
			case 1 :
			return true;
			break;
			default :
			//throw new Exception ( "Error al realizar la insercion en la BD" );
			return false;
			break;
		}
	}
	/**
	 * Selecciona cual es el siguiente miembro del jurado al que se le debe asignar un pincho. 
	 * Elige a un jurado sin pinchos asignados, si no existe ninguno, elige al jurado que tenga menos pinchos asignados
	 *
	 * @throws PDOException si existe un error con la base de datos
	 * @throws Exception Si no se consigue recuperar la id de ningun jurado que todavia no tienen asignaciones y estos existen.
	 * @throws Exception Si no se consigue recuperar la id del jurado con menos pinchos asignados dentro de los que tienen alguno asignado.
	 * @return Juradoprofesional. El miembro del jurado que menos pinchos asignados tiene. False si se produce un error.
	 */
	public function seleccionarJuradoParaAsignar() {
		// Se recupera el primer miembro del jurado que no tiene asignado ningun pincho
		$stmt = $this->db->prepare ( "SELECT * FROM juradoprofesional WHERE NOT EXISTS (SELECT 1 FROM votacionprofesional WHERE  juradoprofesional.idjuradoprofesional = votacionprofesional.juradoprofesional_idjuradoprofesional AND votacionfinalista = 0 AND notavotoprofesional='NULL') limit 1" );
		$stmt->execute ();
		$count = $stmt->rowCount ();
		switch ($count) {
			case 0 : // No hay ningun jurado sin pinchos asignados
			$stmt = $this->db->prepare ( "SELECT juradoprofesional_idjuradoprofesional,Count(*) tot FROM votacionprofesional GROUP BY juradoprofesional_idjuradoprofesional ORDER BY tot LIMIT 1;" );
			$stmt->execute ();
			$juradoProfesionalAsignacion = $stmt->fetch ( PDO::FETCH_ASSOC );
			$stmt = $this->db->prepare ( "SELECT * FROM juradoprofesional WHERE idjuradoprofesional = ?" );
				// Se recupera el valor del juradoProfesional desde su id
			$stmt->execute ( array (
				$juradoProfesionalAsignacion ["juradoprofesional_idjuradoprofesional"] 
				) );
				// Se comprueba que la consulta ha sido correcta
			$count0 = $stmt->rowCount ();
			switch ($count0) {
				case 0 :
				//throw new Exception ( "No se encentra la id del jurado con menos pinchos asignados" );
				return false;
				break;
					case 1 : // La consulta ha sido correcta
					$juradoProfesional = $stmt->fetch ( PDO::FETCH_ASSOC );
					return new Juradoprofesional ( $juradoProfesional ["idjuradoprofesional"], $juradoProfesional ["nombre"], $juradoProfesional ["usuario"], $juradoProfesional ["email"], $juradoProfesional ["password"], $juradoProfesional ["telefono"], $juradoProfesional ["foto"], $juradoProfesional ["descripcion"] );
					break;
					default :
					//throw new Exception ( "Ha surgido un problema al recuperar el jurado profesional con menos pinchos asignados" );
					return false;
					break;
				}
				
				break;
			case 1 : // Se ha seleccionado el primer jurado sin pinchos asignados
			$juradoProfesional = $stmt->fetch ( PDO::FETCH_ASSOC );
			return new Juradoprofesional ( $juradoProfesional ["idjuradoprofesional"], $juradoProfesional ["nombre"], $juradoProfesional ["usuario"], $juradoProfesional ["email"], $juradoProfesional ["password"], $juradoProfesional ["telefono"], $juradoProfesional ["foto"], $juradoProfesional ["descripcion"] );
			break;
			default :
			//throw new Exception ( "Error al calcular el jurado para asignar" );
			return false;
			break;
		}
	}
	/**
	 * Asigna un pincho a un miembro del jurado
	 *
	 * @param Juradoprofesional $jurado jurado con la id al que se le debe asignar un pincho
	 * @param Pincho $pincho pincho con la id que se desea asignar a un jurado
	 * @param int $idAdministrador identificador del administrador asociado al jurado
	 * @throws PDOException si existe un error con la base de datos
	 * @throws Exception si se inserta mas de una tupla en la base de datos
	 * @return boolean. Devuelve true (1) si se ha producido la asignacion, false (0) en caso contrario
	 */
	public function asignarPincho($jurado, $pincho) {
		$stmt = $this->db->prepare ( "INSERT INTO votacionprofesional(votacionfinalista, notavotoprofesional, pincho_idpincho, juradoprofesional_idjuradoprofesional) values (?,?,?,?)" );
		$stmt->execute ( array (
				0, // Para indicar que se trata de una votacion previa
				NULL, // Al realizar la asignacion, la nota otorgada se mantiene a null
				$jurado->get_id (), // $pincho->get_id()
				1 
				) );
		$count = $stmt->rowCount ();
		switch ($count) {
			case 0 :
			return false;
			break;
			case 1 :
			return true;
			break;
			default :
			//throw new Exception ( "Error al realizar la insercion en la BD" );
			return false;
			break;
		}
	}
	
	/**
	 * Valida el login del juradoProfesional
	 *
	 * @param Login $login login con el usuario que se va a loguear
	 * @param string $pass contraseña del juradoProfesional que se loguea 
	 * @throws PDOException si existe un error con la base de datos
	 */
	public function validarLogin($login,$pass){
		$sentencia = $this->db->prepare ("SELECT * from juradoprofesional WHERE usuario=? and password=?");
		$sentencia->execute( array(
			$login,
			$pass
			) );

		$count = $sentencia->rowCount ();
		echo $count;
		switch ($count) {
			case 0 :
			return false;
			break;
			case 1 :
			return true;
			break;
			default :
			//throw new Exception ( "Error al realizar la actualizacion en la BD" );
			return false;
			break;
		}

	}
}
?>
